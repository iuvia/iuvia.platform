"""
Utilities for dealing with the system.

For the moment, we just have an incomplete pythonic systemctl implementation
"""
from __future__ import annotations

import subprocess


class systemctl:
    """
    An interface to the systemctl command that is found on systemd-enabled Linux instances.
    """
    _systemctl = "/usr/bin/systemctl"

    user: systemctl_user

    @classmethod
    def daemon_reload(cls):
        return cls.systemctl("daemon-reload")

    @classmethod
    def enable(cls, unit: str):
        return cls.systemctl("enable", unit)

    @classmethod
    def reload(cls, unit: str):
        return cls.systemctl("reload", unit)

    @classmethod
    def start(cls, unit: str):
        return cls.systemctl("start", unit)

    @classmethod
    def disable(cls, unit: str):
        return cls.systemctl("disable", unit)

    @classmethod
    def stop(cls, unit: str):
        return cls.systemctl("stop", unit)

    @classmethod
    def kill(cls, unit: str, *args):
        return cls.systemctl("kill", unit, *args)

    @classmethod
    def is_active(cls, *units) -> bool:
        try:
            cls.systemctl("is-active", "--quiet", *units)
            return True
        except subprocess.CalledProcessError:
            return False

    @classmethod
    def is_failed(cls, *units) -> bool:
        try:
            cls.systemctl("is-failed", "--quiet", *units)
            return True
        except subprocess.CalledProcessError:
            return False

    @classmethod
    def systemctl(cls, *args) -> bytes:
        return subprocess.check_output([cls._systemctl, *args])


class systemctl_user(systemctl):
    @classmethod
    def systemctl(cls, *args) -> bytes:
        return subprocess.check_output([cls._systemctl, '--user', *args])


systemctl.user = systemctl_user
