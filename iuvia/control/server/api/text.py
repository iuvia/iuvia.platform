from __future__ import annotations
import click
import json
import typing
from asyncio.streams import StreamReader, StreamWriter

from iuvia.control import __version__
if typing.TYPE_CHECKING:
    from iuvia.control.server.server import ControlServer


class OnClientConnected(object):
    def __init__(
        self, reader: StreamReader, writer: StreamWriter, server: ControlServer
    ):
        self.reader = reader
        self.writer = writer
        self.server = server

    @classmethod
    async def create(cls, reader: StreamReader, writer: StreamWriter, server):
        self = cls(reader=reader, writer=writer, server=server)
        await self.begin_session()

    def get_cmd(self, cmd):
        """
        Used internally to signal which commands do exist
        """
        on_cmd = "on_{}".format(cmd)
        if hasattr(self, on_cmd) and callable(getattr(self, on_cmd)):
            return getattr(self, on_cmd)

    async def begin_session(self):
        """
        Begin a session by greeting the client and stating our version.
        Afterwards, await for messages from the client and react to them.
        We read the first line and pass arguments only for the first line.
        Afterwards it's up to each command to implement.

        Only a single command is expected to run here. If there are more
        commands expected to run, those commands may call begin_session to
        restart again.
        """
        self.writer.write(b"iuvia.control\nversion 1\n")
        await self.writer.drain()
        line = await self.reader.readline()
        cmd = line.decode("utf-8").split()[0]
        try:
            if self.get_cmd(cmd):
                await self.get_cmd(cmd)(line)
            else:
                self.writer.write(b"command-unknown ok-thanks-bye\n")
                await self.writer.drain()
        finally:
            await self.close()

    async def on_version(self, _line):
        self.writer.write(__version__.encode("utf-8"))
        await self.writer.drain()

    async def on_register_service(self, line):
        """
        First line will contain the number of bytes in the payload.
        Afterwards, it will be a JSON payload.
        You need to send the payload in EXACTLY those number of bytes.
        """
        click.echo("Service registration incoming:")
        click.echo(line)
        length = int(line.split()[1])
        msg = await self.reader.readexactly(length)
        msg = json.loads(msg)
        name = msg["name"]
        click.echo("Service to be registered is: {}".format(name))
        self.server.register(**msg)
        self.server.reload_nginx()

    async def on_unregister_service(self, line):
        """
        First line will contain the name of the service (subdomain).
        No more lines should be sent.
        """
        name = line.split()[1].decode("utf-8")
        click.echo("Service to be unregistered is: {}".format(name))
        self.server.unregister(name)
        self.server.reload_nginx()

    async def on_install_appimage_coordinates(self, line):
        """
        Install an appimage given a local path
        """

        # Remove first element from line
        coords = line.split(b" ")[1:]
        if len(coords) == 2:
            # No UUID
            codename, version = coords
            await self.server.install_appimage_coordinates(
                codename=codename, version=version, occ=self
            )
        else:
            raise ValueError()

    async def on_uninstall_appimage_coordinates(self, line):
        """
        Install an appimage given a local path
        """
        # Remove first element from line
        codename, version, instance = line.split(b" ")[1:].decode("utf-8")
        await self.server.uninstall_appimage_coordinates(
            codename=codename, version=version, instance=instance, occ=self
        )

    async def on_install_appimage(self, line):
        """
        Install an appimage given a local path
        """
        # Remove first element from line
        appimage_path = line.split(b" ")[1:]
        # Then restate all spaces if there were any, and remove trailing
        # newline
        appimage_path = bytes.join(b" ", appimage_path)[:-1].decode("utf-8")
        await self.server.install_appimage(appimage_path, self)

    async def on_uninstall_appimage(self, line):
        """
        Install an appimage given a local path
        """
        # Remove first element from line
        appimage_path = line.split(b" ")[1:]
        # Then restate all spaces if there were any, and remove trailing
        # newline
        appimage_path = bytes.join(b" ", appimage_path)[:-1].decode("utf-8")
        await self.server.uninstall_appimage(appimage_path, self)

    async def on_debug_registered(self, line):
        """
        Get currently registered services
        """
        click.echo("Asking for currently registered services")
        click.echo(str(self.server.services))
        self.writer.write(str(self.server.services).encode("utf-8"))
        await self.close()

    async def on_debug_nginx_conf(self, line):
        self.writer.write(self.server.nginx_template().encode("utf-8"))
        await self.close()

    async def on_bye(self, _line):
        self.writer.write(b"bye")
        await self.writer.drain()

    async def close(self):
        self.writer.write_eof()
        self.writer.close()
        await self.writer.wait_closed()
