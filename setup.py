#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

from os import path

with open(path.join(path.dirname(path.abspath(__file__)), 'requirements.txt'), 'r') as f:
    requirements = [line.split('#')[0].strip() for line in f.readlines()]

setup(
    install_requires=requirements,
    data_files=[('.', ['requirements.txt'])],
)
