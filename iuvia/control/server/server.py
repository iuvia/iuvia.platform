# iuvia.control.server
# This file is part of iuvia.control

import asyncio
from asyncio.streams import StreamReader, StreamWriter
from datetime import datetime, timezone
import functools
from logging import getLogger, warn
import os
import socket
import subprocess
from typing import Callable, List, Optional, TypeVar
import secrets
import uuid

import click
from sqlalchemy.engine import create_engine
from sqlalchemy.sql.expression import delete
from iuvia.appimages import AppImageOps
from iuvia.control.utils import systemctl
from iuvia.appimages.lifecycle import LifecycleManager
from iuvia.control.server.jinjaparser import render_nginx_conf
from . import sd_glue as systemd
from . import database
from .api.text import OnClientConnected
from .api import base

log = getLogger("iuvia.control.server")

SERVER_CACHE_FILE = "/run/iuvia/control/cache.pickle"
SERVER_DB_FILE = os.environ.get("IUVIA_CONTROL_DB_FILE", "//var/lib/iuvia/control.db")
APPIMAGE_LOCAL_DATA_PATHS = [
    os.environ.get(
        "IUVIA_APPIMAGE_LOCAL_DATA_PATH", "/var/lib/iuvia/boxui/appimages/downloaded"
    ),
    # os.environ.get('IUVIA_APPIMAGE_SYSTEM_DATA_PATH', '/ro/var/lib/iuvia/boxui/appimages'),
]


def check_runtime():
    if os.geteuid() != 0:
        raise Exception("You need to run this as root.")


R = TypeVar('R')


def transactional(f: Callable[..., R]) -> Callable[..., R]:
    def wrapped(self, *args, **kwargs):
        assert isinstance(self, ControlServer), "The method needs to be an instance-method (not classmethod or staticmethod)"
        if self.db.in_transaction():
            with self.db.begin_nested():
                return f(self, *args, **kwargs)
        else:
            with self.db.begin():
                return f(self, *args, **kwargs)
    return functools.wraps(f)(wrapped)


class ControlServer:
    transient_units_prefix = "io.iuvia."
    runtime_unit_path = "/run/systemd/system/"

    def __init__(self):
        engine = create_engine("sqlite://" + SERVER_DB_FILE)
        database.Session.configure(bind=engine)
        print("Database URL: ", engine.url)
        database.Base.metadata.create_all(engine)
        self.db: database._Session = database.Session()

        self.domains = [
            "local.dev.net.iuvia.io",
        ]
        self.refresh_known_appimages(delete_lost_paths=False)
        self.ensure_installed_online()

    @transactional
    def ensure_installed_online(self):
        """Ensure installed services are online"""
        apps = self.db.query(database.AppInstance).filter(
            database.AppInstance.state == database.AppImageVersionState.installed
        )
        for app in apps:
            app_path = app.installed.path
            ops = AppImageOps(app_path)
            plm = LifecycleManager(ops=ops, instance=app.uuid).privileged
            plm.platform_token = self.ensure_platform_token(instance=app, refresh=True)
            try:
                plm.ensure_started()
                socket_path = plm.socket_path
                for endpoint in app.network_endpoints:
                    endpoint: database.AppInstanceNetworkEndpoint
                    if endpoint.protocol == database.AppProtocol.http:
                        endpoint.unix_socket_tcp = socket_path
                        self.db.add(endpoint)
            except Exception as e:
                log.error(
                    "plm.ensure_started failed on AppInstance: %s (%s)",
                    app_path,
                    app,
                    exc_info=e,
                    stack_info=True,
                )

    def ensure_platform_token(self, instance: Optional[database.AppInstance] = None, uuid: Optional[uuid.UUID] = None, refresh: bool = False) -> str:
        assert (instance or uuid) and not (instance and uuid), "you need to provide instance or uuid but not both to ensure_platform_token"
        if uuid:
            instance = self.db.query(database.AppInstance).get(uuid)
            if instance is None:
                raise KeyError("UUID does not correspond to any instance")

        if not instance.platform_token or refresh:
            instance.platform_token = secrets.token_urlsafe(32)
        return instance.platform_token

    def cache_appimage_db(self, appimage_path) -> database.AppImageVersion:
        ops = AppImageOps(appimage_path)
        meta = ops.get_metadata_pb()
        codename = meta.codename
        version = meta.version
        self.db.merge(database.AppImageBase(codename=codename))
        aiv = database.AppImageVersion(
            codename=codename,
            version=version,
            license_code="X-NOTIMPL",
            path=appimage_path,
            signature_state=database.AppSignatureState.none,
            cached_at=datetime.utcnow().astimezone(timezone.utc),
        )
        self.db.merge(aiv)
        return aiv

    def get_local_appimages(self):
        for appimage_dir in APPIMAGE_LOCAL_DATA_PATHS:
            if os.path.isdir(appimage_dir):
                for root, dirs, files in os.walk(appimage_dir):
                    for filename in files:
                        full_path = os.path.join(root, filename)
                        if full_path.lower().endswith(".appimage") and os.access(
                            full_path, mode=os.X_OK
                        ):
                            yield full_path
                    for dir in dirs:
                        runner = os.path.join(root, dir, "AppRun")
                        if os.access(runner, mode=os.X_OK):
                            yield runner

    @transactional
    def refresh_known_appimages(self, delete_lost_paths=False):
        # Walk through the appimage directory
        for full_path in self.get_local_appimages():
            self.cache_appimage_db(full_path)
        for (path,) in self.db.query(database.AppImageVersion).with_entities(database.AppImageVersion.path):
            if not os.path.exists(path) and delete_lost_paths:
                self.db.execute(
                    delete(database.AppImageVersion).where(
                        database.AppImageVersion.path == path
                    )
                )

    @transactional
    def get_known_appimages(self, refresh=True) -> List[database.AppImageVersion]:
        if refresh:
            self.refresh_known_appimages()
        return self.db.query(database.AppImageVersion).all()

    @transactional
    def get_installed_appinstances(self) -> List[database.AppInstance]:
        return self.db.query(database.AppInstance).all()

    async def install_appimage_coordinates(
        self, *, codename, version, instance=None
    ) -> base.ServerInstallResponse:
        with self.db.begin():
            aiv = (
                self.db.query(database.AppImageVersion)
                .filter(
                    database.AppImageVersion.codename == codename,
                    database.AppImageVersion.version == version,
                )
                .first()
            )
            if not aiv:
                return base.CoordinatesDoNotExist(codename=codename, version=version)

            if instance:
                id = uuid.UUID(instance.replace("_", ""))
            else:
                id = uuid.uuid4()
            instance = database.AppInstance(
                uuid=id,
                codename=codename,
                auto_update=True,
                state=database.AppImageVersionState.installed,
                installed_version=version,
            )
            http_endpoint = database.AppInstanceNetworkEndpoint(
                uuid=id,
                subdomain=str(id).replace("-", ""),
                port=0,
                protocol=database.AppProtocol.http,
            )
            self.db.add(instance)
            self.db.add(http_endpoint)

            # Get metadata
            ops = AppImageOps(aiv.path)
            plm = LifecycleManager.create(ops=ops, instance=id).privileged
            try:
                plm.install()
                self.reload_nginx()
                return base.ServerInstallOKResponse(socket_path=plm.socket_path, instance=id)
            except subprocess.CalledProcessError as e:
                return base.ServerInstallFailResponse(return_code=e.returncode, command=e.cmd)
            except FileNotFoundError as e:
                return base.FileNotFound(filename=e.filename)

    async def uninstall_appimage_coordinates(self, *, codename, version, instance) -> base.ServerUninstallResponse:
        with self.db.begin():
            aiv = (
                self.db.query(database.AppImageVersion)
                .filter(
                    database.AppImageVersion.codename == codename,
                    database.AppImageVersion.version == version,
                )
                .first()
            )
            if not aiv:
                return base.CoordinatesDoNotExist(codename=codename, version=version)

            id = uuid.UUID(instance.replace("_", ""))
            instance = self.db.query(database.AppInstance).get(id)
            ops = AppImageOps(instance.installed.path)
            plm = LifecycleManager(ops=ops, instance=id).privileged
            try:
                plm.uninstall()
                self.db.delete(instance)
                self.reload_nginx()
                return base.ServerUninstallOKResponse()
            except subprocess.CalledProcessError as e:
                return base.ServerInstallFailResponse(return_code=e.returncode, command=e.cmd)
            except FileNotFoundError:
                return base.FileNotFound()

    async def install_appimage(self, appimage_path) -> base.ServerInstallResponse:
        # Create in DB
        with self.db.begin():
            aiv = self.cache_appimage_db(appimage_path)
            codename = aiv.codename
            version = aiv.version
            return self.install_appimage_coordinates(codename, version)

    async def uninstall_appimage(self, appimage_path) -> base.ServerUninstallResponse:
        # Get metadata
        ops = AppImageOps(appimage_path)
        plm = LifecycleManager.create(ops).privileged
        try:
            plm.uninstall()
            return base.ServerUninstallOKResponse()
        except subprocess.CalledProcessError as e:
            return base.ServerInstallFailResponse(return_code=e.returncode, command=e.cmd)
        except FileNotFoundError:
            return base.FileNotFound()

    def is_active(self, appimage_path) -> bool:
        ops = AppImageOps(appimage_path)
        plm = LifecycleManager.create(ops).privileged
        return plm.is_active()

    def _socket_path(app_instance: database.AppInstance) -> str:
        app_path = app_instance.installed_version.path
        ops = AppImageOps(app_path)
        plm = LifecycleManager.create(ops, app_instance.uuid).privileged
        return plm.socket_path

    def nginx_template(self):
        services = [
            {
                "name": svc.subdomain,
                "path": "/",
                "default": 0,
                "backend": svc.unix_socket_tcp or self._socket_path(svc.instance),
            }
            for svc in self.db.query(database.AppInstanceNetworkEndpoint)
            .filter(
                database.AppInstanceNetworkEndpoint.protocol
                == database.AppProtocol.http
            )
            .all()
        ]

        return render_nginx_conf(
            tlds=self.domains,
            avahi_hostname="iuvia-io.local",
            services=services,
        )

    def reload_nginx(self):
        if os.geteuid() != 0:
            warn("Not reloading nginx due to insufficient permissions")
            return
        with open("/etc/nginx/nginx.conf", "wb") as f:
            f.write(self.nginx_template().encode("utf-8"))
        systemctl.reload("nginx.service")


_control_server = None


def get_control_server() -> ControlServer:
    global _control_server
    if _control_server is None:
        _control_server = ControlServer()
    return _control_server


async def create_client_connected_cb(reader: StreamReader, writer: StreamWriter):
    return await OnClientConnected.create(
        reader=reader, writer=writer, server=get_control_server()
    )


async def setup_main_loop(loop=None):
    "These are the docs of setup_main_loop"
    from iuvia.control.internal import CONTROL_SOCK_PATH

    sock_dir = os.path.dirname(CONTROL_SOCK_PATH)
    if not os.path.exists(sock_dir):
        os.makedirs(sock_dir)

    systemd.signal_ready()
    fds = systemd.listen_fds(False)
    if fds:  # Activated via systemd.socket
        fds = systemd.SD_LISTEN_FDS_START + fds - 1
        server = await asyncio.streams.start_unix_server(
            client_connected_cb=create_client_connected_cb,
            loop=loop,
            start_serving=True,
            sock=socket.socket(fileno=fds),
        )
    else:
        server = await asyncio.streams.start_unix_server(
            client_connected_cb=create_client_connected_cb,
            path=CONTROL_SOCK_PATH,
            loop=loop,
            start_serving=True,
        )
        os.chmod(CONTROL_SOCK_PATH, 0o666)  # noqa: S103
    await server.serve_forever()


@click.command()
@click.option(
    "--init-nginx/--no-init-nginx",
    help="Initialize nginx.conf on first run",
    is_flag=True,
    default=True,
)
def cli(*, init_nginx: bool = True):
    click.echo("iuvia.control.server")
    check_runtime()
    if init_nginx:
        get_control_server().reload_nginx()
    asyncio.run(setup_main_loop())
