from __future__ import annotations
import typing

if typing.TYPE_CHECKING:
    from iuvia.control.server.server import ControlServer  # noqa: F401


class ServerResponse(object):
    pass


class ServerInstallResponse(ServerResponse):
    pass


class ServerUninstallResponse(ServerResponse):
    pass


class ServerInstallOKResponse(ServerInstallResponse):
    socket_path: str
    instance: str

    def __init__(self, socket_path: str, instance: str):
        self.socket_path = socket_path
        self.instance = instance


class ServerUninstallOKResponse(ServerUninstallResponse):
    pass


class ServerInstallFailResponse(ServerInstallResponse, ServerUninstallResponse):
    def __init__(self, return_code, command):
        self.return_code = return_code
        self.command = command


class CoordinatesDoNotExist(ServerInstallResponse, ServerUninstallResponse):
    def __init__(self, codename=None, version=None):
        self.codename = codename
        self.version = version


class FileNotFound(ServerInstallResponse):
    def __init__(self, filename):
        self.filename = filename

    def __str__(self):
        return 'FileNotFound(filename={filename})'.format(
            filename=self.filename
        )
