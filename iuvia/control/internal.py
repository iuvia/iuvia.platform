import os

CONTROL_SOCK_PATH = os.environ.get(
    "IUVIA_CONTROL_SOCK_PATH", "/run/iuvia/control-v0.sock"
)
