from __future__ import annotations
from logging import warn
from iuvia.appimages import AppImageOps
from iuvia.control.client import API as ICAPI


class PrivilegedLifecycleManager(object):
    @property
    def platform_token(self) -> str:
        """Sets the token that the platform gives the app to communicate with it"""
        raise NotImplementedError("")

    @platform_token.setter
    def platform_token(self, value):
        raise NotImplementedError("")

    def install(self):
        pass

    @property
    def socket_path(self) -> str:
        raise NotImplementedError("")

    def uninstall(self):
        pass

    def is_active(self) -> bool:
        return False

    def ensure_started(self):
        pass


class LifecycleManager(object):
    """
    AppImage Lifecycle Manager class.

    The Lifecycle manager helps communicate through iuvia.control to
    install and remove AppImages.  This should allow in the future to
    change the system interface (whatever we want to use, upstart,
    systemd, or even Docker containers) without changing the user
    code.
    """
    def __init__(self, *, ops: AppImageOps, instance: str):
        self.ops = ops
        self.instance = instance

    @staticmethod
    def create(*, ops: AppImageOps, instance: str) -> LifecycleManager:
        return LifecycleManager(ops=ops, instance=instance)

    @property
    def socket_path(self) -> str:
        return self.privileged.socket_path

    @property
    def privileged(self) -> PrivilegedLifecycleManager:
        # Check if UID=0
        import os
        if os.geteuid() == 0:
            from iuvia.appimages.internal.systemd import PrivilegedSDLifecycleManager
            return PrivilegedSDLifecycleManager(appimage=self.ops, instance=self.instance)
        else:
            warn("We are using the systemd.user implementation, which is not meant for production.")
            from iuvia.appimages.internal.systemd_user import PrivilegedUserlevelSDLifecycleManager
            return PrivilegedUserlevelSDLifecycleManager(appimage=self.ops, instance=self.instance)

    def install(self, subdomain, instance):
        api = ICAPI()
        api.install_appimage(self.ops.path, instance)
        api.register_service(
            name=subdomain,
            unix_socket_http=self.socket_path,
        )

    def uninstall(self, subdomain, instance):
        api = ICAPI()
        api.uninstall_appimage(self.ops.path, instance)
        api.unregister_service(subdomain)
