from __future__ import annotations
from datetime import datetime, timezone
import enum
from typing import Callable

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship, Session as _Session
from sqlalchemy.orm.session import sessionmaker
from sqlalchemy.sql.schema import Column, ForeignKey, ForeignKeyConstraint, PrimaryKeyConstraint
from sqlalchemy.sql.sqltypes import Boolean, DateTime, Enum, Integer, LargeBinary, Numeric, SmallInteger, String, Text

from .uuid_type import UUID


Base = declarative_base()
Session = sessionmaker()  # type: Callable[..., _Session]


class AppDeveloper(Base):
    __tablename__ = 'appimage_developers'
    fingerprint = Column(String(256), primary_key=True)
    public_key = Column(Text, nullable=True)
    name = Column(String(255), nullable=True)
    primary_email = Column(String(255), nullable=True)
    trusted_until = Column(DateTime(timezone=True), nullable=True)


class AppImageBase(Base):
    """All updates of the same AppImage share the same instance of this object."""
    __tablename__ = 'appimage_metadata'
    codename = Column(String(512), primary_key=True)
    developer_fingerprint = Column(String(256), ForeignKey(AppDeveloper.fingerprint), nullable=True)


class AppImageVersionState(enum.Enum):
    uninstalled = 0x00
    installed = 0x01
    install_pending = 0x02
    install_failed = 0x04
    installing = 0x07
    uninstalling = 0x20
    uninstall_failed = 0x40
    updating = 0x17


class AppInstance(Base):
    """AppImages might be installed multiple times."""
    __tablename__ = 'app_instance'
    uuid = Column(UUID, primary_key=True)
    codename = Column(String(512), ForeignKey(AppImageBase.codename, ondelete='CASCADE'))
    auto_update = Column(Boolean, default=True, nullable=False)
    state = Column('state', Enum(AppImageVersionState))
    installed_version = Column(String(16), ForeignKey('appimage_versions.version', ondelete="SET NULL"), nullable=True)
    platform_token = Column(String(512), nullable=True)

    installed: AppImageVersion = relationship(
        'AppImageVersion',
        primaryjoin="and_(AppInstance.codename==AppImageVersion.codename, AppInstance.installed_version==AppImageVersion.version)",
        backref=backref("installed_instances", cascade="all, delete-orphan")
    )
    available_versions = relationship(
        'AppImageVersion',
        primaryjoin="AppInstance.codename==AppImageVersion.codename",
        backref='base',
        foreign_keys=(codename,),
    )

    network_endpoints = relationship('AppInstanceNetworkEndpoint', back_populates='instance', cascade="all,delete")


class AppProtocol(enum.Enum):
    tcp = 1
    udp = 2
    http = 3
    imap = 4
    smtp = 5


class AppInstanceNetworkEndpoint(Base):
    __tablename__ = 'app_instance_net_endpoints'
    uuid = Column(UUID, ForeignKey(AppInstance.uuid, ondelete='CASCADE'), primary_key=True)
    subdomain = Column(String(64), primary_key=True)
    port = Column(SmallInteger, primary_key=True, default=-1)
    protocol = Column(Enum(AppProtocol), primary_key=True)
    tor_service_key = Column(LargeBinary, nullable=True, default=None)
    unix_socket_tcp = Column(String(64), nullable=True, default=None)

    instance = relationship('AppInstance', back_populates='network_endpoints')


class AppSignatureState(enum.Enum):
    invalid = -1
    none = 0
    unverified = 1
    verified = 2
    trusted = 3


class AppImageVersion(Base):
    __tablename__ = 'appimage_versions'
    codename = Column(String(512), ForeignKey(AppImageBase.codename, onupdate="CASCADE"), primary_key=True)
    version = Column(String(16), primary_key=True)
    license_code = Column(String(32), nullable=False, default="X-UNKNOWN")
    path = Column(Text, nullable=False, unique=True)
    signature_state = Column(Enum(AppSignatureState), default=AppSignatureState.none)
    cached_at = Column(DateTime(timezone=True), nullable=False, default=lambda: datetime.utcnow().astimezone(timezone.utc))

    icons = relationship('CachedIcon', back_populates='appimage')

    def __str__(self):
        return 'AppImageVersion(codename={codename}, version={version})'.format(
            codename=self.codename,
            version=self.version,
        )


class CachedIcon(Base):
    __tablename__ = 'cached_icons'
    codename = Column(String(512))
    version = Column(String(16))
    height = Column(Integer)
    width = Column(Integer)
    dpi = Column(Numeric(precision=8, scale=2), default=1)
    content = Column(LargeBinary, nullable=False)
    cached_at = Column(DateTime(timezone=True), nullable=False, default=lambda: datetime.utcnow().astimezone(timezone.utc))

    __table_args__ = (
        PrimaryKeyConstraint(codename, version, height, width, dpi),
        ForeignKeyConstraint((codename, version), (AppImageVersion.codename, AppImageVersion.version))
    )
    appimage = relationship('AppImageVersion', back_populates='icons')


class LocalizedAppString(Base):
    __tablename__ = 'l10n_app_strings'
    codename = Column(String(512), ForeignKey(AppImageVersion.codename))
    version = Column(String(16), ForeignKey(AppImageVersion.version))
    lang = Column(String(32), nullable=True, index=True)
    name = Column(String(255), nullable=False)
    short_description = Column(Text, nullable=True)
    long_description = Column(Text, nullable=True)
    __table_args__ = (
        PrimaryKeyConstraint(codename, version, lang),
    )


class AppLauncher(Base):
    __tablename__ = 'app_launchers'
    uuid = Column(UUID, primary_key=True)
    codename = Column(String(512), ForeignKey(AppImageVersion.codename))
    version = Column(String(16), ForeignKey(AppImageVersion.version))
    platform = Column(String(32), nullable=True)
    on_activate = Column(String(32), nullable=True, index=True)

    icons = relationship('CachedLauncherIcon', backref='launcher')
    strings = relationship('CachedLauncherStrings', backref='launcher')


class CachedLauncherIcon(Base):
    __tablename__ = 'cached_launcher_icons'
    launcher_uuid = Column(UUID, ForeignKey(AppLauncher.uuid), primary_key=True)
    height = Column(Integer, primary_key=True)
    width = Column(Integer, primary_key=True)
    dpi = Column(Numeric(precision=8, scale=2), primary_key=True, default=1)
    content = Column(LargeBinary, nullable=False)
    cached_at = Column(DateTime(timezone=True), nullable=False, default=lambda: datetime.utcnow().astimezone(timezone.utc))


class CachedLauncherStrings(Base):
    __tablename__ = 'cached_launcher_strings'
    launcher_uuid = Column(UUID, ForeignKey(AppLauncher.uuid), primary_key=True)
    lang = Column(String(32), nullable=True, index=True)
    name = Column(String(255), nullable=True)
    description = Column(Text, nullable=True)
