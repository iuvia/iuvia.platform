IUVIA Platform
=====

This is the reference IUVIA Platform implementation.

:License: AGPLv3


Development environment
-----------------------

Instructions are currently under development.

Deployment environment
----------------------

In order to correctly deploy iuvia_platform, a versioned wheel should be created. Until we reach the point of maturity of releasing wheels in a public server, just add the dependency to the repo as a pip requirement when installing.


Basic Operations
----------------

Enable iuvia.control.server
^^^^^^^^^^^^^^^^^^^^^^^^^^^

After installing iuvia_platform, do run:

::
   # python -m iuvia.control.server

Retrieve and install AppImages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

TODO

Debug installed AppImages
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

You can see the current structure written on the SQLite file directly with sqlite3 on the command line.


Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ pytest


Contributing
------------

This repository is part of IUVIA. Contribution guidelines are going to be explained in the near future here.

Meanwhile please get in touch via email and we'll set you up.
