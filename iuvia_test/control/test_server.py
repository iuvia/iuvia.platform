import os

from iuvia.appimages.ops import AppImageOps
from iuvia.control.server import database

import iuvia.control.server.server
from iuvia.control.server.server import get_control_server

iuvia.control.server.server.APPIMAGE_LOCAL_DATA_PATHS = [
    os.path.join(os.path.dirname(__file__), 'fixtures_appimages')
]
iuvia.control.server.server.SERVER_DB_FILE = ''  # so that it is sqlite:// (in-memory)


class TestServer:
    def test_server_finds_local_appimages(self):
        server = get_control_server()
        local_appimages = list(server.get_local_appimages())
        assert len(local_appimages) == 1
        ops = AppImageOps(local_appimages[0])
        assert ops.get_metadata_pb().codename == 'io.iuvia.demo.helloworld'

    def test_server_bootstraps_with_local_appimages(self):
        server = get_control_server()
        with server.db.begin():
            db_appimages = server.db.query(database.AppImageVersion).all()
        assert len(db_appimages) == 1
        assert db_appimages[0].codename == 'io.iuvia.demo.helloworld'
        assert db_appimages[0].version == '0.0.1'
