import asyncio
from datetime import datetime
from typing import Any, List, Literal, Optional, Union
import click
import os

from fastapi import FastAPI
from fastapi.params import Depends
import fastapi.security
import hypercorn.asyncio
import hypercorn.config
from pydantic.main import BaseModel

from .. import sd_glue as systemd
from ..api import base
from ..server import check_runtime, get_control_server
from ... import __version__

app = FastAPI()
bearer_token = fastapi.security.HTTPBearer()


@app.get("/")
async def root():
    return {
        "server": "IUVIA Platform",
        "version": __version__,
    }


@app.post("/appimages/install")
async def appimages_install_credentials(codename: str, version: str, instance: Optional[str] = None):
    res = await get_control_server().install_appimage_coordinates(codename=codename, version=version, instance=instance)
    if isinstance(res, base.ServerInstallOKResponse):
        return {"msg": "OK", "status": "installed", "codename": codename, "version": version, "instance": res.instance, "socket_path": res.socket_path}
    elif isinstance(res, base.ServerInstallFailResponse):
        return {"msg": "fail", "error": "Execution failed", "return_code": res.return_code, "failed_command": res.command}
    elif isinstance(res, base.CoordinatesDoNotExist):
        return {"msg": "fail", "error": "Coordinates do not exist"}
    else:
        return {"msg": "fail", "error": "Unknown error: {} (type: {})".format(res, type(res))}


@app.post("/appimages/uninstall")
async def appimages_uninstall_credentials(codename: str, version: str, instance: str):
    res = await get_control_server().uninstall_appimage_coordinates(codename=codename, version=version, instance=instance)
    if isinstance(res, base.ServerUninstallOKResponse):
        return {"status": "ok", "appimage_state": "uninstalled", "codename": codename, "version": version, "instance": instance}
    elif isinstance(res, base.ServerInstallFailResponse):
        return {"status": "fail", "error": "Execution failed", "return_code": res.return_code, "failed_command": res.command}
    elif isinstance(res, base.CoordinatesDoNotExist):
        return {"status": "fail", "error": "Coordinates do not exist"}
    else:
        return {"status": "fail", "error": "Unknown error: {} (type: {})".format(res, type(res))}


class AppImageVersionModel(BaseModel):
    codename: str
    version: str
    license_code: str
    path: str
    signature_state: Any
    cached_at: datetime


class AppInstanceModel(BaseModel):
    uuid: Any
    codename: str
    auto_update: bool
    state: Any
    installed_version: Optional[str]
    platform_token: Optional[str]


class AppImageListModel(BaseModel):
    status: Union[Literal['ok'], Literal['fail']]
    items: List[AppImageVersionModel]


class AppInstanceListModel(BaseModel):
    status: Union[Literal['ok'], Literal['fail']]
    items: List[AppInstanceModel]


@app.get("/appimages", response_model=AppImageListModel)
async def appimages_list_known():
    appimages = get_control_server().get_known_appimages()
    appimages = [
        dict((col, getattr(appimage, col)) for col in appimage.__table__.columns.keys())
        for appimage in appimages
    ]
    return {"status": "ok", "items": appimages}


@app.get("/appimages/installed", response_model=AppInstanceListModel)
async def appimages_installed():
    appimages = get_control_server().get_installed_appinstances()
    appimages = [
        dict((col, getattr(appimage, col)) for col in appimage.__table__.columns.keys())
        for appimage in appimages
    ]
    return {"status": "ok", "items": appimages}


@app.get("/test")
async def test_endpoint(token: str = Depends(bearer_token)):  # noqa: B008
    return {"msg": "Success. Token is {}".format(token)}


async def setup_main_loop():
    "These are the docs of setup_main_loop"
    from ...internal import CONTROL_SOCK_PATH

    sock_dir = os.path.dirname(CONTROL_SOCK_PATH)
    print("GOT SOCK PATH: ", CONTROL_SOCK_PATH)
    if not os.path.exists(sock_dir):
        os.makedirs(sock_dir)

    config = hypercorn.config.Config()

    systemd.signal_ready()
    fds = systemd.listen_fds(False)
    if fds:  # Activated via systemd.socket
        fds = systemd.SD_LISTEN_FDS_START + fds - 1
        config.bind = ["fd://{:d}".format(fds)]
    else:
        config.bind = ["unix:" + CONTROL_SOCK_PATH]
    if os.environ.get('IUVIA_CONTROL_SERVER_HTTP_DEBUG_BIND'):
        config.bind.append('{}'.format(os.environ.get('IUVIA_CONTROL_SERVER_HTTP_DEBUG_BIND')))
    return await hypercorn.asyncio.serve(app, config)


@click.command()
@click.option(
    "--init-nginx/--no-init-nginx",
    help="Initialize nginx.conf on first run",
    is_flag=True,
    default=True,
)
def cli(*, init_nginx: bool = True):
    click.echo("iuvia.control.server")
    if init_nginx:
        check_runtime()
        get_control_server().reload_nginx()
    asyncio.run(setup_main_loop())


if __name__ == "__main__":
    cli()
